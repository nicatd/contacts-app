# Contacts Application
This website is a contacts list app to add, delete, edit contacts 

## Installation

Clone repository

```sh
   git clone https://gitlab.com/nicatd/contacts-app.git
```

## Usage
This app is website, so for debugging you must install Webstorm or Visual Studio Code.
If you want you see project, click on the link below

<a href=" https://604f0ca35052f6480a21eac0--pedantic-khorana-2c7af2.netlify.app/#/">Contacts Application</a>


## Used technologies
Vue, Vuex, Vuex-persistedstate, CSS

## License
[MIT](https://gitlab.com/nicatd/contacts-app/-/blob/master/LICENSE)



# contacts-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
